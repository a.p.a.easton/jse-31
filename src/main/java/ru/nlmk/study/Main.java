package ru.nlmk.study;

import ru.nlmk.study.service.MyService;

import java.math.BigInteger;
import java.util.Scanner;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static  final Logger logger = Logger.getLogger(Main.class.getName());

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        MyService myService = new MyService();
        logger.info("Commands:");
        logger.info("1. sum.");
        logger.info("2. factorial.");
        logger.info("3. fibonacci.");
        logger.info("4. exit.");
        boolean iterator = true;
        try {
            while (iterator) {
                String firstNumber;
                String secondNumber;
                String number;
                long result;
                logger.info("Enter a command");
                String command = scanner.nextLine();
                switch (command) {
                    case ("sum"):
                        logger.info("sum");
                        logger.info("enter first number: ");
                        firstNumber = scanner.nextLine();
                        logger.info("enter a second number: ");
                        secondNumber = scanner.nextLine();
                        result = myService.sum(firstNumber, secondNumber);
                        logger.info(String.format("sum %s + %s = %d",firstNumber, secondNumber, result));
                        break;
                    case ("factorial"):
                        logger.info("factorial");
                        logger.info("enter a number: ");
                        number = scanner.nextLine();
                        logger.info("enter a count of threads");
                        String threadCount = scanner.nextLine();
                        long startTimeOneThread = System.currentTimeMillis();
                        BigInteger bigInteger = myService.factorial(number, "1");
                        long timeOneThread = System.currentTimeMillis() - startTimeOneThread;
                        long startTimeSeveralThreads = System.currentTimeMillis();
                        bigInteger = myService.factorial(number, threadCount);
                        long timeSeveralTreads = System.currentTimeMillis() - startTimeSeveralThreads;
                        logger.info(String.format("factorial %s = %d", number, bigInteger));
                        if(timeSeveralTreads < timeOneThread){
                            logger.info(String.format("Several threads time %d is better than one thread time %d", timeSeveralTreads, timeOneThread));
                        }
                        else{
                            logger.info(String.format("Several threads time %d is worse than one thread time %d", timeSeveralTreads, timeOneThread));
                        }
                        break;
                    case ("fibonacci"):
                        logger.info("fibonacci");
                        logger.info("enter a number: ");
                        number = scanner.nextLine();
                        Long[] fibonacci = myService.fibonacci(number);
                        logger.info(String.format("fibonacchi %s = %s", number, Arrays.toString(fibonacci)));

                        break;
                    case ("exit"):
                        logger.info("Exit");
                        iterator = false;
                        break;
                    default:
                        logger.info("Command not found");
                }
            }
        } catch(IllegalArgumentException exeption){
            logger.log(Level.SEVERE, exeption.getMessage());
        }
    }
}
