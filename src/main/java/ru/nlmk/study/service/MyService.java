package ru.nlmk.study.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class MyService {

    public long sum(String arg1, String arg2){
        int a, b;
        try {
            a = Integer.parseInt(arg1);
            b = Integer.parseInt(arg2);
            return a + b;
        } catch (NumberFormatException e){
            throw new IllegalArgumentException("Incorrect number format");
        }
    }

    public BigInteger factorial(String arg, String threads){
        try{
            int threadsCount = Integer.parseInt(threads);
            int diapazon = Integer.parseInt(arg);
            int number = diapazon/threadsCount;
            BigInteger result = BigInteger.valueOf(1);
            List<factorialThread> threadList = new ArrayList<>();
            for(int i = 1; i <= threadsCount-1; i++){
                threadList.add(new factorialThread(number*(i-1)+1, number*(i)));
                threadList.get(i-1).start();
            }
            if(diapazon == number){
                threadList.add(new factorialThread(1, diapazon));
            }
            else{
                threadList.add(new factorialThread((threadsCount-1)*number+1, diapazon));
            }
            threadList.get(threadsCount-1).start();
            for(int i = 0; i < threadsCount; i++){
                threadList.get(i).join();
                result = result.multiply(threadList.get(i).getResult());
            }
            return result;
        } catch(NumberFormatException | ArithmeticException | InterruptedException e){
            throw new IllegalArgumentException("Incorrect number format");
        }
    }

    public Long[] fibonacci(String arg){
        try {
            int n = Integer.parseInt(arg);
            if (n <= 0) {
                throw new IllegalArgumentException("Number is less then 0.");
            }
            if(Math.sqrt(5*n*n-4) % 1 == 0 || Math.sqrt(5*n*n+4) % 1 == 0){//если условие выполняется, то число явялется числом Фибоначчи.
                List<Long> result = new ArrayList<>();
                result.add(1L);
                result.add(1L);
                for(int i = 2, j = 2; j <= n; i++, j = (int)(result.get(i-2)+result.get(i-1))){
                    result.add(result.get(i-2)+result.get(i-1));
                }
                Long[] l = new Long[result.size()];
                for(int i = 0; i < result.size(); i++){
                    l[i] = result.get(i);
                }
                return l;
            } else{
                throw new IllegalArgumentException();
            }
        } catch(NumberFormatException e){
            throw new IllegalArgumentException("Incorrect number format");
        }
    }

}
