package ru.nlmk.study.service;

import java.math.BigInteger;

public class factorialThread extends Thread{
    private Integer start;
    private Integer end;
    private BigInteger result;

    public factorialThread(int start, int end) {
        this.start = start;
        this.end = end;
        this.result = BigInteger.valueOf(start);
    }

    public BigInteger getResult() {
        return result;
    }

    @Override
    public void run() {
        for(int i = start; i < end; i++) {
            result = result.multiply(BigInteger.valueOf(i+1));
        }
    }
}
